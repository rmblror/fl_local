\begin{minipage}[t]{0.074\linewidth}
\end{minipage}
\begin{minipage}[t]{0.85\linewidth}
\begin{minipage}[t]{0.7\linewidth}
{\noindent\Large \textbf{(% ingredient.name %)}}
\end{minipage}
\hfill
\begin{minipage}[t]{0.28\linewidth}
\raggedleft
\textbf{(% profile.cost_string.format(ingredient.cost_per_kilo) %)/kg}
\end{minipage}

\vspace{3em}

\begin{minipage}{0.45\textwidth}
\includegraphics[width=\textwidth]{figs/ingredients/(% '{}.pdf'.format(ingredient.name.replace(' ','_')) %)}
\end{minipage}
\hfill
\begin{minipage}{0.54\textwidth}
\raggedleft
\begin{tabular}{rl}
& Per 100g \\
{% for key in ingredient.nutrition_dict_display %}
(% key %) & (% '{:.2f}'.format(ingredient.nutrition_dict_display[key] / ingredient.grams * 100) %) \\
{% endfor %}
\end{tabular}
\end{minipage}
\end{minipage}

\pagebreak
