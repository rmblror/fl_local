\documentclass[a4paper]{article}
%\usepackage{multicol}
\usepackage[bottom]{footmisc}
\usepackage{graphicx}
\usepackage{array,ragged2e}
\newcolumntype{R}[1]{>{\RaggedLeft}p{(%'#'%)1}}
\usepackage[hmargin=1.5cm]{geometry}

\begin{document}
{% for recipe in profile.recipes_main %}
\input{recipes/(% recipe %)}
{% endfor %}
{% if profile.make_dependencies %}

\pagebreak
{\Huge Dependencies}
\pagebreak

{% for recipe in profile.recipes_dependency %}
\input{recipes/(% recipe %)}
{% endfor %}
{% endif %}


{% if profile.make_ingredient_pages %}
\pagebreak
{\Huge Ingredients}
\pagebreak

{% for ingredient in profile.ingredient_objects %}
\input{ingredients/(% ingredient.replace(' ','_') %)}
{% endfor %}
{% endif %}


\end{document}
