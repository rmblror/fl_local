# Recipes for freelunch

This is my collection of recipes that can be used with my project, freelunch, which can be found at https://gitlab.com/rmblror/freelunch

If this repository is cloned into the main directory in freelunch, then its contents will be given priority over the defaults in freelunch.
That is, any templates, profiles, or recipes with the same filename will be read from this repository instead of from the defaults in freelunch.

# A solution for the future

I admit, this isn't a particularly good long-term solution for storing and sharing recipes.
For now, the motivation is simply that I want to develop and maintain the freelunch repository such that the recipes distributed outside of the repository will work seamlessly.
Because it's only me adding recipes here, this is fine enough for now, but a more robust, decentralized, user-driven sharing tool is the model I am hoping for in the long run.

# The recipes

These are all recipes which I have made, and which I have modified slightly from the source.
There is no rhyme or reason to what order I add them in and which kinds of recipes are represented.
This is simply the collection of new meals that I happen to make chronologically from this point in time and which happen to turn out somewhat well.

# Copying

My understanding of copyright law with repsect to recipes is that a recipe itself (i.e. the ingredients and steps abstractly) cannot be copyrighted, but the particular wording of a recipe can be, as well as any media generated in association with it (such as photos, and perhaps the pdf document here).
No recipe here was copied verbatim from the original source which I used as a starting point, so I believe I have the right to redistribute them in this way.
If I am in error, and you, the reader, are the owner of intellectual property which I am distributing, please let me know, and I will respond as soon as possible.
Otherwise, consider this explicit permission to copy and to redistribute not only these recipes, but also the particular wording and any possible media without attribution under the creative commons license CC0.

# Contributing

If you have recipes to contribute, please fork the repository, add them, and make a pull request.
If this process dissuades you from contributing, please let me know so that I will prioritize finding a better solution sooner.
I don't like taking photos of my food, but I recognize that it can often be important in order to give an idea of what the result should be.
Therefore, I plan to add the display of such photos as a feature sometime.
If you would like to share a photo of a recipe that you add, please include it in the recipe folder with the same namebase, i.e. my_cool_recipe.yml for the recipe and my_cool_recipe.png for the photo, and it will be included in an update to freelunch.
Please do not include a photo that you do not wish to be redistributed under these permissive terms.